using System;
using System.Data;
using System.Data.SqlClient;
using System.Data.SqlTypes;
using System.IO;
using System.Net;
using System.Text;
using System.Threading.Tasks;
using Microsoft.SqlServer.Server;

public partial class StoredProcedures
{
    [Microsoft.SqlServer.Server.SqlProcedure]
    public static void ActualizarPrecioDelProductoNidux(string codigo, float precio)
    {
        SqlPipe pipe = SqlContext.Pipe;

        string token = Convert.ToString(ObterResultado("select TokenNidux from informaci�n;", null, CommandType.Text));

        string url = "https://api.nidux.net/v1/products/" + codigo + "/price";

        string data = "{\"price\":" + precio.ToString("f2") + "}";

        int maxRecursion = 3;
        int curentRecursion = 1;
        do
        {
            curentRecursion++;

            string requestReponse = DoRequest("PATCH", url, data, token, "text/plain");

            DataTable dtError = EjecutarDataTable("spVerificarQueElJsonNoEsUnError",
                cmd => { cmd.Parameters.Add(new SqlParameter("Json", SqlDbType.NVarChar)).Value = requestReponse; },
                CommandType.StoredProcedure);

            if (dtError.Columns.Contains("Code") && dtError.Rows.Count > 0)
            {
                if (Convert.ToInt32(dtError.Rows[0]["Code"]) == 403)
                {
                    EjecutarConsulta("spActualizarTokenNidux");
                }
                else
                {
                    pipe.Send($"Codigo {dtError.Rows[0]["Code"]}, {dtError.Rows[0]["Message"]}");
                    break;
                }
            }
            else
            {
                pipe.Send("Se ha actualizado el precio");
                break;
            }

            if (curentRecursion > maxRecursion)
            {
                pipe.Send("Se realizaron el n�mero m�ximo de reintentos");
                break;
            }
        } while (true);

        // ReSharper disable once PossibleNullReferenceException
        // pipe.Send();
    }

    [Microsoft.SqlServer.Server.SqlProcedure]
    public static void ActualizarStockDelProductoNidux(string codigo, float stock)
    {
        SqlPipe pipe = SqlContext.Pipe;

        string token = Convert.ToString(ObterResultado("select TokenNidux from informaci�n;", null, CommandType.Text));

        string url = "https://api.nidux.net/v1/products/" + codigo + "/stock";

        string data = "{\"stock\":" + stock.ToString("f2") + "}";

        int maxRecursion = 3;
        int curentRecursion = 1;
        do
        {
            curentRecursion++;

            string requestReponse = DoRequest("PATCH", url, data, token, "text/plain");

            DataTable dtError = EjecutarDataTable("spVerificarQueElJsonNoEsUnError",
                cmd => { cmd.Parameters.Add(new SqlParameter("Json", SqlDbType.NVarChar)).Value = requestReponse; },
                CommandType.StoredProcedure);

            if (dtError.Columns.Contains("Code") && dtError.Rows.Count > 0)
            {
                if (Convert.ToInt32(dtError.Rows[0]["Code"]) == 403)
                {
                    EjecutarConsulta("spActualizarTokenNidux");
                }
                else
                {
                    pipe.Send($"Codigo {dtError.Rows[0]["Code"]}, {dtError.Rows[0]["Message"]}");
                    break;
                }
            }
            else
            {
                pipe.Send("Se ha actualizado el stock");
                break;
            }

            if (curentRecursion > maxRecursion)
            {
                pipe.Send("Se realizaron el n�mero m�ximo de reintentos");
                break;
            }
        } while (true);

        // ReSharper disable once PossibleNullReferenceException
        // pipe.Send();
    }

    [Microsoft.SqlServer.Server.SqlProcedure]
    public static void HacerRequestWeb(string metodo, string url, string dataToSend, string bearerToken,
        string contentType)
    {
        SqlPipe pipe = SqlContext.Pipe;

        // ReSharper disable once PossibleNullReferenceException
        string jsonResponse = DoRequest(metodo, url, dataToSend, bearerToken, contentType);

        SqlDataRecord record = new SqlDataRecord(
            new SqlMetaData("JsonResponse", SqlDbType.NVarChar, -1));

        pipe.SendResultsStart(record);

        record.SetString(0, jsonResponse);

        pipe.SendResultsRow(record);

        pipe.SendResultsEnd();
    }

    private static string DoRequest(string method, string requestUrl, string dataToSend, string bearerToken,
        string contentType = "application/json")
    {
        try
        {
            System.Net.ServicePointManager.SecurityProtocol =
                SecurityProtocolType.Tls12 | SecurityProtocolType.Tls11 | SecurityProtocolType.Tls;

            HttpWebRequest request = (HttpWebRequest) WebRequest.Create(requestUrl);

            request.KeepAlive = false;
            request.ProtocolVersion = HttpVersion.Version10;

            request.Method = method;

            request.Timeout = 12000;
            request.ContentType = contentType;

            request.Proxy = WebRequest.GetSystemWebProxy();
            if (request.Proxy != null)
                request.Proxy.Credentials = System.Net.CredentialCache.DefaultCredentials;

            if (bearerToken != null)
            {
                request.Headers.Add("Authorization", "Bearer " + bearerToken);
            }

            // request.Headers.Add("X-PW-Application", "developer_api");
            // request.Headers.Add("X-PW-AccessToken", "{AccessToken}");
            // request.Headers.Add("X-PW-UserEmail", "{UserEmail}");

            if (dataToSend != null)
            {
                var postBytes = Encoding.ASCII.GetBytes(dataToSend);

                request.ContentLength = postBytes.Length;

                Stream requestStream = request.GetRequestStream();

                requestStream.Write(postBytes, 0, postBytes.Length);
            }

            HttpWebResponse response = (HttpWebResponse) request.GetResponse();

            // Get the stream associated with the response.
            Stream receiveStream = response.GetResponseStream();

            // Pipes the stream to a higher level stream reader with the required encoding format. 
            StreamReader readReceiveStream = new StreamReader(receiveStream, Encoding.UTF8);

            string readData = readReceiveStream.ReadToEnd();

            readReceiveStream.Close();

            response.Close();

            return readData;
        }
        catch (WebException e)
        {
            // Get the stream associated with the response.
            Stream receiveStream = e.Response.GetResponseStream();

            // Pipes the stream to a higher level stream reader with the required encoding format. 
            StreamReader errorStream = new StreamReader(receiveStream, Encoding.UTF8);

            string readData = errorStream.ReadToEnd();

            errorStream.Close();

            return readData;
        }
        catch (Exception e)
        {
            return e.ToString();
        }
    }


    #region SqlUtils

    public static DataTable EjecutarDataTable(string nombreDelSp, Action<SqlCommand> ejecutarAntes = null,
        CommandType commandType = CommandType.StoredProcedure)
    {
        var dataTable = new DataTable();

        using (var sqlConnection = new SqlConnection(@"context connection=true"))
        {
            try
            {
                sqlConnection.Open();

                using (var sqlCommand = new SqlCommand())
                {
                    sqlCommand.CommandTimeout = 0;
                    sqlCommand.CommandText = nombreDelSp;
                    sqlCommand.CommandType = commandType;
                    sqlCommand.Connection = sqlConnection;

                    ejecutarAntes?.Invoke(sqlCommand);

                    var sqlDataAdapter = new SqlDataAdapter(sqlCommand);

                    sqlDataAdapter.Fill(dataTable);

                    return dataTable;
                }
            }
            catch (SqlException e)
            {
                Console.WriteLine(e.ToString());
                return dataTable;
            }
            catch (Exception e)
            {
                Console.WriteLine(e.ToString());
                return dataTable;
            }
            finally
            {
                sqlConnection.Close();
                sqlConnection.Dispose();
            }
        }
    }

    public static object ObterResultado(string consulta, Action<SqlCommand> ejecutarAntes = null,
        CommandType commandType = CommandType.StoredProcedure)
    {
        using (var sqlConnection = new SqlConnection(@"context connection=true"))
        {
            try
            {
                sqlConnection.Open();

                using (var sqlCommand = new SqlCommand())
                {
                    sqlCommand.CommandText = consulta;
                    sqlCommand.CommandType = commandType;
                    sqlCommand.Connection = sqlConnection;

                    ejecutarAntes?.Invoke(sqlCommand);

                    var sqlDataReader = sqlCommand.ExecuteReader();

                    if (sqlDataReader.HasRows)
                    {
                        sqlDataReader.Read();

                        return sqlDataReader.GetValue(0);
                    }
                    else
                    {
                        return null;
                    }
                }
            }
            catch (SqlException e)
            {
                Console.WriteLine(e.ToString());
                return null;
            }
            catch (Exception e)
            {
                Console.WriteLine(e.ToString());
                return null;
            }
            finally
            {
                sqlConnection.Close();
                sqlConnection.Dispose();
            }
        }
    }

    public static int EjecutarConsulta(string sqlConsulta, Action<SqlCommand> ejecutarAntes = null,
        CommandType commandType = CommandType.StoredProcedure)
    {
        using (var sqlConnection = new SqlConnection(@"context connection=true"))
        {
            try
            {
                // sqlConnection.ConnectionString = CadenaConexion;
                sqlConnection.Open();

                using (var sqlCommand = new SqlCommand())
                {
                    sqlCommand.CommandText = sqlConsulta;
                    sqlCommand.CommandTimeout = 0;
                    sqlCommand.CommandType = CommandType.StoredProcedure;
                    sqlCommand.Connection = sqlConnection;

                    ejecutarAntes?.Invoke(sqlCommand);

                    return sqlCommand.ExecuteNonQuery();
                }
            }
            catch (SqlException e)
            {
                Console.WriteLine(e.ToString());
                return -1;
            }
            catch (Exception e)
            {
                Console.WriteLine(e.ToString());
                return -1;
            }
            finally
            {
                sqlConnection.Close();
                sqlConnection.Dispose();
            }
        }
    }

    #endregion
}